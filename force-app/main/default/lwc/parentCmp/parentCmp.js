import {
    LightningElement,
    track
} from 'lwc';

export default class ParentCmp extends LightningElement {

    @track tooltipContent = 'Tooltip Content';
}