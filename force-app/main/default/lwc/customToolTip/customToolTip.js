import {
    LightningElement,
    track,
    api
} from 'lwc';

export default class CustomToolTip extends LightningElement {

    nubbinClasses = {
        top: [
            "slds-nubbin_top", "slds-nubbin_top-left", "slds-nubbin_top-right",
            "slds-nubbin_top-left-corner", "slds-nubbin_top-right-corner"
        ],
        right: [
            "slds-nubbin_right", "slds-nubbin_right-top", "slds-nubbin_right-bottom",
            "slds-nubbin_right-top-corner", "slds-nubbin_right-bottom-corner"
        ],
        bottom: [
            "slds-nubbin_bottom", "slds-nubbin_bottom-left", "slds-nubbin_bottom-right",
            "slds-nubbin_bottom-left-corner", "slds-nubbin_bottom-right-corner"
        ],
        left: [
            "slds-nubbin_left", "slds-nubbin_left-top", "slds-nubbin_left-bottom",
            "slds-nubbin_left-top-corner", "slds-nubbin_left-bottom-corner"
        ]
    }

    /**
     * Supported popover variants.
     * base - warning - error - feature
     */
    supportedVariants = {
        base: "",
        error: "slds-popover_error",
        feature: "slds-popover_feature",
        warning: "slds-popover_warning"
    };

    @track nubbinClassInternal = 'slds-nubbin_bottom-left'

    // To track so that we do the calculations only the first time.
    @track isInitialized = false;
    @track idVal;

    /**
     * Content APIs
     */
    @api heading;
    @api content;
    @api toolTipId;
    @api isRichText = false;

    /**
     * You can provide the nubbin class to allow for full slds-popover position control. Only valid classed accepted
     */
    @api
    get nubbinClass() {
        return this.nubbinClassInternal;
    }
    set nubbinClass(nubbinClass) {
        if (this.nubbinClasses.top.includes(nubbinClass) || this.nubbinClasses.right.includes(nubbinClass) || this
            .nubbinClasses
            .bottom.includes(nubbinClass) || this.nubbinClasses.left.includes(nubbinClass)) {
            this.isInitialized = false
            this.nubbinClassInternal = nubbinClass;
        }
    }

    /**
     * UI Control APIs
     */
    @track variantInternal = 'info';
    @track iconNameInternal = 'utility:info_alt';

    @api size = "small"
    @api hideIcon = false;
    @api sectioncss = 'position:absolute;width:200px';
    @api ishoverable = false;
    @api iconVariant = 'info';

    @api
    get variant() {
        return this.variantInternal;
    }
    set variant(variant) {
        if (this.supportedVariants[variant]) {
            this.variantInternal = variant;
        }
    }

    @api
    get iconName() {
        return this.iconNameInternal;
    }
    set iconName(iconName) {
        this.iconNameInternal = iconName;
    }

    @api
    show() {
        let popoverElem = this.template.querySelector('.slds-popover');
        if (popoverElem.classList.contain('slds-popover_hide')) {
            this.showPopover();
        }
    }

    @api
    hide() {
        let popoverElem = this.template.querySelector('.slds-popover');
        popoverElem.classList.add('slds-popover_hide');
    }

    /**
     * This method handles the click event on the icon, but it has nothing to do with the click itself.
     * Calculates the intersection point where the icon and nubbin of the popover align.
     * For the icon, depending on which side the popover lies to the icon, calculates the position on the following basis.
     *      1.  Let's assume icon size is 2,2. Considering a eucledian space with left-top of the icon as 0,0 
     *          and positive directions to the right and bottom of direction, the four points starting from top side in a circular direction would be
     *          (1,0 ) -> (2,1) -> (1,2) -> (0,1)
     *      2.  Adds 5 more pixels in the appropriate direction to add spacing between nubbin and icon
     * 
     * For the popover, it does not matter where it currently lies. We need to find the poistion of the tip of the nubbin, 
     * and transform that position to the intersection point found above. For that, the algorithm uses the "width" and "height" of the 
     * popover component, and the "left" , "right" , "top" and "bottom" of the "before" element i.e. the nubbin. 
     * After finding the relative position of the nubbin to the popover, updates the popover position such that the tip of the 
     * nubbin is placed at the intersection point discovered for the icon.
     * 
     */

    showPopover = event => {
        let popoverElem = this.template.querySelector('.slds-popover');
        let isPresent = !(popoverElem.classList.toggle('slds-popover_hide'));
        if (!this.isInitialized && isPresent) {
            let iconElement = this.template.querySelector('lightning-icon');
            // Icon Parameters
            let iconHeight = iconElement.offsetHeight,
                iconWidth = iconElement.offsetWidth,
                iconTop = iconElement.offsetTop,
                iconLeft = iconElement.offsetLeft;
            // Popover parameters. Subtracting 2 accounting for border
            let popoverHeight = popoverElem.offsetHeight - 2,
                popoverWidth = popoverElem.offsetWidth - 2;
            // Computer Style for before element
            let computedStyle = window.getComputedStyle(popoverElem, 'before');
            //
            let x1;
            let y1;
            let x2;
            let y2;

            if (this.nubbinClasses.top.includes(this.nubbinClass)) {
                // Nubbin on top, i.e. Popover below the Icon
                x1 = iconLeft + iconWidth / 2 - 1;
                y1 = iconHeight + iconTop + 5;

                x2 = parseInt(computedStyle.left);
                y2 = parseInt(computedStyle.top);
            } else if (this.nubbinClasses.right.includes(this.nubbinClass)) {
                // Nubbin on Right, i.e. Popover left of the Icon
                x1 = iconLeft - 5;
                y1 = iconTop + iconHeight / 2;

                x2 = popoverWidth - parseInt(computedStyle.right);
                y2 = parseInt(computedStyle.top);
            } else if (this.nubbinClasses.bottom.includes(this.nubbinClass)) {
                // Nubbin on Bottom, i.e. Popover top of the Icon
                x1 = iconLeft + iconWidth / 2 - 1;
                y1 = iconTop - 5;
                x2 = parseInt(computedStyle.left);
                y2 = popoverHeight - parseInt(computedStyle.bottom);
            } else if (this.nubbinClasses.left.includes(this.nubbinClass)) {
                // Nubbin on Left, i.e. Popover Right of the Icon
                x1 = iconLeft + iconWidth + 5;
                y1 = iconTop + iconHeight / 2;
                x2 = parseInt(computedStyle.left);
                y2 = parseInt(computedStyle.top);
            }

            // Update Nubbin location to the intersection point for the Icon by transforming the popover itself.
            popoverElem.style.left = (x1 - x2) + 'px';
            popoverElem.style.top = (y1 - y2) + 'px';
            this.isInitialized = true;
        }
        this.dispatchEvent(new CustomEvent('getid', {
            detail: this.toolTipId
        }));
    }

    closePopover = event => {
        this.template.querySelector('.slds-popover').classList.toggle('slds-popover_hide');
    }

    renderedCallback() {
        let popoverElement = this.template.querySelector('.slds-popover');
        popoverElement.classList.add(this.nubbinClass);
        popoverElement.classList.add(this.supportedVariants[this.variantInternal]);
        this.isInitialized = false;
    }
}